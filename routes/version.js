var downloadableVersion='1.0.1';
var description="Webview, modal windows, button play/stop";

var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {
	res.render('version', { version: downloadableVersion, description: description });
});

module.exports = router;
