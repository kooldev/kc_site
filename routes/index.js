var express = require('express');
var router = express.Router();
//console.log("window",window);
//console.log("navigator.browserLanguage",navigator.browserLanguage);
/*if (navigator.browserLanguage)
var language = navigator.browserLanguage;


else
var language = navigator.language;*/
var lang = "";

function changeLanguage(myLang){
	var menuJson = require('../texts/'+myLang+'/menu.json');
	menuJsonText = JSON.parse(JSON.stringify(menuJson));

	var indexJson = require('../texts/'+myLang+'/index.json');
	indexJsonText = JSON.parse(JSON.stringify(indexJson));
	indexJsonText.lang = myLang;
	indexJsonText.menuText = menuJsonText;

	var learnJson = require('../texts/'+myLang+'/learn.json');
	learnJsonText = JSON.parse(JSON.stringify(learnJson));
	learnJsonText.lang = myLang;
	learnJsonText.menuText = menuJsonText;
	learnJsonText.pageName = "/learn";

	var principlesJson = require('../texts/'+myLang+'/principles.json');
	principlesJsonText = JSON.parse(JSON.stringify(principlesJson));
	principlesJsonText.lang = myLang;
	principlesJsonText.menuText = menuJsonText;
	principlesJsonText.pageName = "/principles";

	var glossaryJson = require('../texts/'+myLang+'/glossary.json');
	glossaryJsonText = JSON.parse(JSON.stringify(glossaryJson));
	glossaryJsonText.lang = myLang;
	glossaryJsonText.menuText = menuJsonText;
	glossaryJsonText.pageName = "/glossary";

	var tutoJson = require('../texts/'+myLang+'/tuto.json');
	tutoJsonText = JSON.parse(JSON.stringify(tutoJson));
	tutoJsonText.lang = myLang;
	tutoJsonText.menuText = menuJsonText;

	var faqJson = require('../texts/'+myLang+'/faq.json');
	faqJsonText = JSON.parse(JSON.stringify(faqJson));
	faqJsonText.lang = myLang;
	faqJsonText.menuText = menuJsonText;

	var hofJson = require('../texts/'+myLang+'/hof.json');
	hofJsonText = JSON.parse(JSON.stringify(hofJson));
	hofJsonText.lang = myLang;
	hofJsonText.menuText = menuJsonText;
	
	var versionsJson = require('../texts/'+myLang+'/versions.json');
	versionsJsonText = JSON.parse(JSON.stringify(versionsJson));
	versionsJsonText.lang = myLang;
	versionsJsonText.menuText = menuJsonText;

	var partnersJson = require('../texts/'+myLang+'/partners.json');
	partnersJsonText = JSON.parse(JSON.stringify(partnersJson));
	partnersJsonText.lang = myLang;
	partnersJsonText.menuText = menuJsonText;

	var subscribeJson = require('../texts/'+myLang+'/subscribe.json');
	subscribeJsonText = JSON.parse(JSON.stringify(subscribeJson));
	subscribeJsonText.lang = myLang;

	console.log("Changed lang",myLang);
}
function findDefaultLanguage(myLang){
	var okLang = myLang.split("-")[0];
	if(okLang!==lang){
		if(okLang!=="fr"){
			okLang = "en"
		}
		lang = okLang;
		changeLanguage(lang);
	}
}
function checkCookieLang(myReq,myRes){
  var cookie = myReq.cookies.language;
    //console.log('myReq.headers',JSON.stringify(myReq.headers));
    //console.log('myReq.headers accept language',myReq.headers['accept-language']);
  if (cookie === undefined){
	//changeLanguage(lang);
	if(myReq.headers['accept-language'] !== undefined){
		findDefaultLanguage(myReq.headers['accept-language'].split(",")[0]);
	}else{
		findDefaultLanguage("fr-FR");
	}
    // no: set a new cookie
    recordCookie(myRes,lang);
    console.log('cookie language created successfully',lang);
  }else{
    // yes, cookie was already present 
    //console.log('cookie language exists', cookie);
    //console.log('cookie language actuel', lang);
    if(cookie!==lang){
    	lang = cookie;
		changeLanguage(lang);
    	recordCookie(myRes,lang);
    }
    //myRes.cookie('language',"en", { maxAge: -10, httpOnly: true });
  } 
  return myRes;
}
function recordCookie(res,lang){
    res.cookie('language',lang, { path:'/', maxAge: 900000, httpOnly: true });
}
function changeCookieLang(myRes,myLang){
    console.log("changeCookieLang",myLang);
	changeLanguage(myLang);
    recordCookie(myRes,myLang);
  	return myRes;
}
/* GET home page. */
router.get('/', function(req, res, next) {
	res = checkCookieLang(req,res);
	console.log("/ --------------- close " ,indexJsonText.menuText.prefs.close)
  	res.render('index', indexJsonText);
});

router.get('/learn', function(req, res, next) {
	//console.log('req Cookies: ', req.cookies);
	res = checkCookieLang(req,res);
  	res.render('learn', learnJsonText);
});

router.get('/principles', function(req, res, next) {
	res = checkCookieLang(req,res);
  	res.render('principles', principlesJsonText);
});

router.get('/glossary', function(req, res, next) {
	res = checkCookieLang(req,res);
	//glossaryJsonText.pageName = req.path;
  	res.render('glossary', glossaryJsonText);
});

router.get('/tuto', function(req, res, next) {
	res = checkCookieLang(req,res);
  res.render('tuto', tutoJsonText);
});

router.get('/faq', function(req, res, next) {
	res = checkCookieLang(req,res);
  res.render('faq', faqJsonText);
});

router.get('/partners', function(req, res, next) {
	res = checkCookieLang(req,res);
  res.render('partners', partnersJsonText);
});
router.get('/hof', function(req, res, next) {
	res = checkCookieLang(req,res);
  res.render('hof', hofJsonText);
});
router.get('/versions', function(req, res, next) {
	res = checkCookieLang(req,res);
  res.render('versions', versionsJsonText);
});
router.get('/updates/:language/:version', function(req, res, next) {
	res = checkCookieLang(req,res);
	var myLang = "en"
	if(req.params.language == "fr"){
		myLang = "fr";
	}
	var updateJson = require('../texts/'+myLang+'/updates/versions_'+req.params.version+'.json')
	var updateJsonText = JSON.parse(JSON.stringify(updateJson));
	updateJsonText.lang = lang;
	updateJsonText.menuText = menuJsonText;
  	res.render('updates/version'+req.params.version, updateJsonText);
});

router.post('/changeLang', function(req, res, next) {
	changeLanguage(req.body.lang);
    recordCookie(res,req.body.lang);
	//console.log("/ --------------- close " ,indexJsonText.menuText.prefs.close);
	res.send("done");
});
router.get('/download/:os/:name', function(req, res, next) {
	//console.log("req.params",req.params);
	var nameFile = "";
	if(req.params.os == "mac"){
		nameFile = req.params.name + ".dmg";
	}else{
		nameFile = req.params.name + ".exe";
	}
  	var filename = './public/download/'+req.params.os+"/"+nameFile;
	//console.log("filename",filename);
  	res.download(filename, nameFile, function(err){
	  	if (err) {
	    	// Handle error, but keep in mind the response may be partially-sent
	    	// so check res.headersSent
	  		console.log("not good",err);
	  	} else {
	  		console.log("merci d'avoir téléchargé Kool Capture !!!!");
	    	// decrement a download credit, etc.
	  	}
	});
});


var MailsNewsletter = require("../models/mailsNewsletter.js")

MailsNewsletter.find(function(err, emails) {
	console.log("begin emails",emails);
});

router.post('/subscribe', function(req,res) {
	console.log("success",req.body.email);
	MailsNewsletter.find({email:req.body.email},function(err, emails) {
        if (err)
            res.send(err);

		res = checkCookieLang(req,res);
        if(emails.length == 0){
        	console.log("not found");
          	MailsNewsletter.create({email:req.body.email}, function(err, glossary) {
          		subscribeJsonText.email = req.body.email;
			  	res.render('subscribe/subscribed', subscribeJsonText);
				MailsNewsletter.find(function(err, emails) {
					console.log("emails",emails);
				});
		    });
        }else{
	       	console.log("mail already there, not created");
  			res.render('subscribe/subscribedAlready', subscribeJsonText);
        }
    });
});
router.get('/unsubscribe/:id', function(req, res, next) {
	var myId = req.params.id;
	res = checkCookieLang(req,res);
	MailsNewsletter.findById(myId, function (err, item) {
	    if (!item) {
	  		res.render('subscribe/unsubscribedAlready', subscribeJsonText);
	    }else{
	        subscribeJsonText.email = item.email;
			MailsNewsletter.remove({ email:item.email }, function (err) {
			  	if (err) return handleError(err);
				MailsNewsletter.find(function(err, emails) {
					console.log("after remove emails",emails);
				});
			    console.log("mail found, removed");
				res = checkCookieLang(req,res);
	  			res.render('subscribe/unsubscribed', subscribeJsonText);
			});
	    }
	});
});

/*router.get('/listNewsletter', function(req, res, next) {

  MailsNewsletter.find(function(err, mailsNewsletter) {
  	mailsNewsletter.forEach(function(email) {
  		console.log(mailsNewsletter.email);
  	});
      res.render("subscribe/listNewsletter", { newsletterList: mailsNewsletter });
  });

});*/
module.exports = router;
