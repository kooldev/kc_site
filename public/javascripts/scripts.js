
function changeLanguage(myRadio) {
	console.log('window.location ' + window.location);
	var myLoc = window.location;
	console.log('New value: ' + myRadio.value);
	var infos = {"lang":myRadio.value};
	var myJsonString = JSON.stringify(infos);
	$.ajax({
        type: "POST",
        url: "/changeLang",
        data: myJsonString,
        contentType:"application/json; charset=utf-8"
    }).done(function(retorno){
    	//console.log("retorno",retorno)
		//console.log('window.location ' + window.location);
       	window.location.href = myLoc; 
    });
};
function verifMail(champ)
{
   var regex = /^[a-zA-Z0-9._-]+@[a-z0-9._-]{2,}\.[a-z]{2,4}$/;
   if(!regex.test(champ))
   {
      return false;
   }
   else
   {
      return true;
   }
}
function testEmail(info) {
    if(verifMail(info.value)){
        //$('#submitMail').addClass('validEmail');
        $('#submitMail').removeClass('invalidEmail');
    }else{
        //$('#submitMail').removeClass('validEmail');
        $('#submitMail').addClass('invalidEmail');
    }
};